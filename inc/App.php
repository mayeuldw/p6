<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 08/10/2018
 * Time: 10:10
 */

namespace dw_rpb_roleplayingbook;


class App
{
    protected $option;
    private $version;

    protected $pluginName = 'RollingBook';

    public function __construct(string $mainFile, string $version)
	{
	    include_once __DIR__.'/3rd-party/cmb2/init.php';
	    //include_once __DIR__.'/3rd-party/cmb2-field-faiconselect-master/iconselect.php';
	    include_once __DIR__.'/3rd-party/chosen/init.php';

		include_once __DIR__.'/classes/Option.php';
		include_once __DIR__.'/classes/OptionPage.php';
		include_once __DIR__.'/classes/EntryPostType.php';
        include_once __DIR__.'/classes/CMBLanguage.php';
        include_once __DIR__.'/classes/CMBSocialMedia.php';
		include_once __DIR__.'/classes/FontAwesome.php';
		include_once __DIR__.'/classes/ImportPostType.php';
        include_once __DIR__.'/classes/EntrySearch.php';

        $this->version = $version;
		$this->option = new Option($this->pluginName, $mainFile, $version);
		new OptionPage      ($this->option);
		new EntryPostType   ($this->option);
        new CMBLanguage     ($this->option);
        new CMBSocialMedia  ($this->option);
		new FontAwesome     ($this->option);
		new EntrySearch     ($this->option);
		new ImportPostType  ($this->option);


        register_activation_hook($mainFile, [$this, 'activate']);
        register_deactivation_hook($mainFile, [$this, 'deactivate']);
        add_action('admin_init',[$this, 'upgrader']);
    }



	public function activate()
	{
        // TODO : Add on activate action
	}

	public function deactivate()
	{
        // TODO : Add on deactivate action
	}

	public function upgrader()
    {
        $current_version = $this->option->get('version');
        if($this->version === $current_version) return ;

        if($current_version === null) {
            $this->fistInstall();
        }
        else {
            $this->upgrade();
        }
        $this->option
            ->set('version', $this->version)
            ->save();
    }

    protected function fistInstall()
    {
        // TODO : Add on first install action
    }

    protected function upgrade()
    {
        // TODO : Add on upgrade action
    }
}