<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 16/11/2018
 * Time: 14:32
 */

?>
<h3>Ultimate CSV Importer</h3>
<p>
    Ce plugin n'est pas livré avec un outils d'importation intégré. Tout plugin d'importation gérant les 'custom post type'
    devrais convenir pour importer des documents types tableau.
    Toutefois certains champs du plugin ne sont pas évocateur (IDs par exemple)
    et peuvent gêner la créations des liens colonnes-données de l'importation de document.
</p>
<p>
    Le plugin propose une intégration du plugin 'Ultimate CSV Importer', pour rentrer les champs non évocateur plus clair.
    La version Free d'Ultimate CSV Importer suffit au besoin simple d'importation.
    Pour importer une document avec Ultimate CSV Importer suivre les instructions suivantes :
<ol>
    <li>Le fichier de donnée doit être au format CSV et être encodé en UTF-8 (voir ci-dessous)</li>
    <li>Dans l'interface d'administration de WordPress : Ultimate CSV Importer > Import / Mise à jour</li>
    <li>Uploader votre fichier de donnée, puis choisir dans la liste déroulante (Import each record as) : 'rpb_entry'</li>
    <li>Passer à la page suivante</li>
    <li>Choisir le mode 'Drag & Drop'</li>
    <li> cliquer/déposer les champs du tableau de droite dans les champs du post. <br>
        <ul>
            <li>Le champs tag se trouve dans le groupe : 'Taxonomies, Catégories & Tags'</li>
            <li>Les champs langues et liens se trouvent dans le groupe : 'Custom Fields'</li>
        </ul>
    </li>
    <li>Une fois tous les champs assigné passer à la page suivantes.</li>
    <li>Les autres pages ne présente pas d'intérêt vous pouvez les valider</li>
    <li>Une fois l'import commencé un rapport vous donnera le résultat de l'importation. Si l'importation ne va pas plus loin que la 1re ligne, cliquer sur 'pause' puis 'resume'</li>
</ol>
</p>

<h3>Format des données</h3>
<p>Ultimate CSV Importer ne gère que les fichiers CSV encodé en UTF-8.
    Malheureusement Excel ne propose pas ce format a=sous cet encodage ...</p>
<p>Deux solutions sont alors disponible : </p>
<ol>
    <li>Utiliser un autre tableur comme Libre Office (plus simple)</li>
    <li>Utiliser Excel mais transcoder le ficher</Li>
</ol>

<h4>Avec Libre Office</h4>
<ol>
    <li>
        Ouvrir le CSV (ex celui en Pièce jointe) qui est déjà encodé correctement avec Libre Office.
        Lors de l’import faire attentions aux deux zone rouges dans l’images ci-dessous :<br>
        <img src="<?= plugins_url( '', __FILE__ ).'/import1.png'?>" alt="pop-up a l'ouverture du fichier" >
    </li>
    <li>
        Modifier puis enregistrer le fichier.
    </li>
    <li>
        Importer le fichier tel quel dans WordPress
    </li>
</ol>

<h4>Avec Excel et Notepad++</h4>
<ol>
    <li>
        N’ouvrir dans Excel que des CSV qui sont encodé en ASCII (format par défaut).
    </li>
    <li>
        Les modifier et les sauvegarder normalement
    </li>
    <li>
        Ouvrir le fichier a importer avec Notepad++
    </li>
    <li>
        Transcoder le fichier
        <br>
        <img src="<?= plugins_url( '', __FILE__ ).'/import2.png'?>" alt="menu notepad++" >
    </li>
    <li>
        Le sauvegarder sous (pour ne pas le recouvrir par erreur avec Excel
    </li>
    <li>
        Importer le nouveau fichier dans WordPress.
    </li>
</ol>

