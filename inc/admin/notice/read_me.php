<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 16/11/2018
 * Time: 14:32
 */

?>
<h3>I. Configuration du plugin</h3>
<p>
    Avant d'utiliser le plugin il est nécessaire de créer les options nécessaires à l'affichage des entrées de l'annuaire.
    En plus de la gestion des langues et des liens le plugin propose les options suivantes :
    <ul>
        <li>
            Slug des articles <br>
            permet de choisir le début de l'url des posts : [MonSite]/[slug]/[slugDeLArticle].
            Le slug de l'article est modifiable dans l'éditeur d'article.
            <br>
            <b>Note : après une modification du slug il est nécessaire de mettre à jour les permaliens.</b>
        </li>
        <li>Feuille de style par défaut <br>
            le plugin propose une mise en page sommaire des éléments propre au plugin.
            Vous pouvez supprimer a mise en page par défaut en décochant ce champ.
        </li>
        <li>
            Font Awesome <br>
            Pour l'affichage des liens il est nécessaire d'utiliser la police de caractère 'Font Awesome' (v4.7).
            Si cette police de caractères est déjà présente dans votre site vous pouvez choisir de ne pas la charger à nouveau.
        </li>

    </ul>
</p>

<h3>II. Gestion des langues</h3>
<p>
    Les articles peuvent posséder une ou plusieurs langues.
    Pour ajouter une nouvelle langue il vous faut renseigner les champs suivants :
</p>
<pre>
    - Nom, utilisé dans le site public comme titre de l'élément (visible au survol)
	- Image, affichée dans le template du post
</pre>
<p>
    Il est possible de modifier, supprimer ou de changer l'ordre des langues.
</p>
<p style="font-weight: bold">
    Attention, la suppression d'une langue est définitive.<br>
    Pour restaurer une langue supprimée, il sera nécessaire de recréer la langue supprimée puis de re-cocher
    l'option correspondante dans tous les articles déjà écrit.
</p>

<h3>III. Gestion des liens</h3>
<p>
    Les articles peuvent posséder une ou plusieurs liens.
    Pour ajouter un nouveau lien il vous faut renseigner les champs suivants  :
</p>
<pre>
	- Nom, information réservé a la zone d'administration.
	- Url de base, début d'URL commune à tous les éléments de ce type de lien (Ex. pour Twitter : "https://wwww.twitter.com/")
	- Préfixe, début du nom de l'éléments au survol (Ex. Twitter : "@")
	- Description, utilisé dans le site public comme titre de l'élément (visible au survol)
	- Icone, affichée dans le template du post
</pre>
<p>
    Il est possible de modifier, supprimer ou de changer l'ordre des liens
</p>
<p style="font-weight: bold">
    Attention, la suppression d'un lien est définitif.<br>
    Pour restaurer un lien supprimé, il sera nécessaire de recréer le lien supprimé puis de re-cocher
    l'option correspondante dans tous les articles déjà écrit.
</p>
