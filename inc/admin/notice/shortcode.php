<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 16/11/2018
 * Time: 14:32
 */

?>
<h3>Shortcode pour l'affichage de l'article</h3>
<p>
    Pour afficher les langues, les liens et les tags d'un article le plugin met a disposition 3 shortcodes:
<pre>
      - rpb_post_tags
      - rpb_post_languages
      - rpb_post_links
    </pre>
Ces trois shortcodes fonctionnent de la manière suivante :
</p>
<table class="widefat fixed">
    <thead>
    <tr>
        <th>Shortcode</th>
        <th>Action</th>
        <th>Classe de référence</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>rpb_post_languages</td>
        <td>Affiche les images des langues coché pour cet article</td>
        <td>rpb_flag</td>
    </tr>
    <tr>
        <td>rpb_post_links</td>
        <td>Affiche les icones des liens actif pour cet article</td>
        <td>rpb_link</td>
    </tr>
    <tr>
        <td>rpb_post_tags</td>
        <td>Affiche les tags sélectionnés pour cet article</td>
        <td>rpb_tag</td>
    </tr>
    </tbody>
</table>
<p>
    Chaque sortcode va afficher une liste d'élément.
    il est possible de personnaliser l'affichage via une feuille de style en utilisant la classe de référence.
    De plus chaque shotcode propose une option : le choix du séparateur s'il y a plusieurs éléments à afficher.
<pre>
        [rpb_post_languages glue=" - "]
        [rpb_post_links glue=", "]
        [rpb_post_tags glue=" / "]
    </pre>
</p>

<h3>Sortcode pour l'affichage du formulaire de recherche</h3>
Pour afficher le formulaire de recherche il faut utiliser le shortcode rpb_search_form.
Pour faciliter le travail d'integration, lors de la soumission du formulaire les options sont automatiquement pré-chargé
dans les pages de resultats generé. Il n'est donc pas necessaire de les remetre dans le template d'affichage des résultats.
Les options du shortcode sont les suivantes :
<pre>
    [rpb_search_form {random} {hide_languages} {hide_links} {hide_tags}]
</pre>
<table class="widefat fixed">
    <thead>
    <tr>
        <th>Option</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>random</td>
        <td>Affiche un article aléatoire parmis tous les resultats disponible pour la recherche demandé</td>
    </tr>
    <tr>
        <td>hide_languages</td>
        <td>Masque les langues dans le selecteur</td>
    </tr>
    <tr>
        <td>hide_links</td>
        <td>Masque les liens dans le selecteur</td>
    </tr>
    <tr>
        <td>hide_tags</td>
        <td>Masque les tags dans le selecteur</td>
    </tr>
    </tbody>
</table>


<h3>Sortcode pour l'affichage des résultats de la recherche</h3>
Pour afficher les resultats de la recherche il faut utiliser le shortcode rpb_search_result.
Ce short code s'utilise sans parametres, les options doivent etre defini dans le shortcode du formulaire de recherche.
<pre>
    [rpb_search_result]
</pre>

