<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 16/11/2018
 * Time: 14:32
 */

?>
<h3>Templates par défaut</h3>
<p>
    Le plugin est livré avec des templates par défaut.
    Pour utiliser un template, le plugin va vérifier l'existante d'un fichier spécifique (voir ci dessous)
    dans le répertoire du thème actif du site.
    Ci le fichier cherché existe alors il sera utilisé ; sinon c'est le template par défaut du plugin qui sera utilisé.
</p>
<p>
    Pour changer le template par défaut du plugin pour l'un de vos propres templates il vous suffit donc de créer le fichier correspondant,
    et d'y écrire votre template.
</p>

<table class="widefat fixed">
    <thead>
    <tr>
        <th>Nom</th>
        <th>Fichier correspondant (chemin depuis la racine du thème)</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Affichage de l'article</td>
        <td>/single-rpb_entry.php</td>
    </tr>
    <tr>
        <td>
            Affichage de la page de résultat<br>
            <i>Utilisé lors de la soumission du formulaire</i>
        </td>
        <td>/search-rpb_entry.php</td>
    </tr>
    <tr>
        <td>
            Affichage des résultats de la recherche<br>
            <i>Utilisé pour le shortcode [rpb_search_result]</i>
        </td>
        <td>/search-rpb_entry-result.php</td>
    </tr>
    </tbody>
</table>