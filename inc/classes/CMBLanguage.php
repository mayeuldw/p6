<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 10/10/2018
 * Time: 17:06
 */

namespace dw_rpb_roleplayingbook;


class CMBLanguage
{
    private $option;

	public function __construct(Option $option)
    {
	    $this->option = $option;
        add_action( 'cmb2_admin_init', [$this, 'registerMetaBox']);
	    add_shortcode('rpb_post_languages', [$this, 'displayLanguages']);
    }

	private function optionId() {
		return $this->option->getPluginPrefix().'language';
	}

    public function registerMetaBox()
    {
	    $base_id = $this->optionId();
        $cmb = new_cmb2_box( array(
            'id'            => $base_id,
            'title'         => __( 'Langues', 'cmb2' ),
            'object_types'  => [$this->option->getPostType()],
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => true
        ) );

	    $languages = $this->getLanguageSettings();
	    foreach($languages as $k => $language) {
		    $cmb->add_field([
			    'name'      => $language,
			    'id'        => $base_id.'_group_'.$k,
			    'type'      => 'checkbox',
		    ]);
	    }
    }

    protected function getLanguageSettings()
    {
	    $languages = $this->option->getPluginOption('language_group');
	    $result = [];
        foreach ($languages as $language) {
            $result[$language['id']] = $language['name'];
        }
        return $result;
    }

	function displayLanguages($args = [])
	{
		global $post;
		if( $args === '') {
			$args = [];
		}

		$result = [];
		$languages = $this->option->getPluginOption('language_group');
		foreach($languages as $k => $language) {
			if(!isset($language['name']) || $language['name'] === '') {
				continue;
			}

			$language['key'] = $this->optionId().'_group_'.$language['id'];
			$language['value'] = get_post_meta($post->ID, $language['key'], true );
			if($language['value'] === '') {
				continue;
			}
			$result[] = '<img class="rpb_flag" src="'.$language['icon'].'" alt="'.$language['name'].'" title="'.$language['name'].'">';
		}
		return implode($args['glue'] ?? ' ', $result);
	}
}