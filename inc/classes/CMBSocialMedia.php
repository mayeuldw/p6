<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 10/10/2018
 * Time: 17:06
 */

namespace dw_rpb_roleplayingbook;


class CMBSocialMedia
{
    private $option;

    public function __construct(Option $option)
    {
	    $this->option = $option;
        add_action( 'cmb2_admin_init', [$this, 'registerMetaBox']);
	    add_shortcode('rpb_post_links', [$this, 'displayLinks']);
    }

    private function optionId() {
		return $this->option->getPluginPrefix().'social_media';
    }

    public function registerMetaBox()
    {
    	$base_id = $this->optionId();
        $cmb = new_cmb2_box([
            'id'            => $base_id,
            'title'         => __('Réseaux Sociaux', 'cmb2' ),
            'object_types'  => [$this->option->getPostType()], // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => false
        ]);

        $links = $this->getLinksSettings();
        foreach($links as $k => $link) {
            $cmb->add_field([
	            'name'      => $link,
	            'label'      => $link,
                'id'        => $base_id.'_group_'.$k,
                'type'      => 'text',
            ]);
        }
    }

    protected function getLinksSettings()
    {
        $links = $this->option->getPluginOption('link_group');
        $result = [];
        foreach($links as $link) {
            $result[$link['id']] = '<i class="'.$link['icon'].'"></i> '.$link['name'];
        }
        return $result;
    }

	function displayLinks($atts = [])
	{
		global $post;
		if($atts === '') {
			$atts = [];
		}

		$links = $this->option->getPluginOption('link_group');
		$result = [];
		foreach($links as $k => $link) {
			if(!isset($link['name']) || $link['name'] === '') {
				continue;
			}
			$link['key'] = $this->optionId().'_group_'.$link['id'];
			$link['value'] = get_post_meta($post->ID, $link['key'], true );
			if($link['value'] === '') {
				continue;
			}
			$result[] = '<a class="rpb_link" href="'.$link['baseUrl'].trim($link['value']).'" title="'.$link['desc'].' : '.$link['namePrefix'].$link['value'].'" target="_blank">'.
			                '<i class="'.$link['icon'].'"></i>'.
			            '</a>';
		}

		return implode($atts['glue'] ?? ' ', $result);
	}
}