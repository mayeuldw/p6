<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 09/10/2018
 * Time: 17:08
 */

namespace dw_rpb_roleplayingbook;


class EntryPostType
{
    private $option;

	public function __construct(Option $option)
    {
        $this->option = $option;
        add_action( 'init', [$this, 'registerPostType']);
        add_action( 'init', [$this, 'registerTagTaxonomy']);
        add_filter( 'single_template', [$this, 'renderSingle']);
	    add_shortcode('rpb_post_tags', [$this, 'displayTags']);
	    add_action('wp_head', [$this, 'addDefaultCSS']);
    }

    public function registerPostType()
    {
        $args = [
            'label'             => __($this->option->getPluginName()),
            'public'            => true,
            'show_ui'           => true,
            'capability_type'   => 'post',
            'hierarchical'      => false,
            'rewrite'           => array('slug' => $this->option->getPluginOption('slug'),'with_front' => false),
            'query_var'         => true,
            'menu_icon'         => 'dashicons-groups',
            'supports'          => [
                'title',
                'editor',
                'excerpt',
                'trackbacks',
                'custom-fields',
                'comments',
                'revisions',
                'thumbnail',
                'author',
                'page-attributes',
            ]
        ];
        register_post_type( $this->option->getPostType(), $args );
    }

    public function registerTagTaxonomy()
    {
        $labels = array(
            'name'              			=> _x( 'Catégories', 'taxonomy general name'),
            'singular_name'     			=> _x( 'Catégorie', 'taxonomy singular name'),
            'search_items'      			=> __( 'Rechercher'),
            'all_items'        				=> __( 'Tous les tags'),
            'edit_item'         			=> __( 'Editer le tag'),
            'update_item'       			=> __( 'Mettre à jour le tag'),
            'add_new_item'     				=> __( 'Ajouter un nouveau tag'),
            'new_item_name'     			=> __( 'Nom'),
            'separate_items_with_commas'	=> __( 'Séparer les tag avec une virgule'),
            'menu_name'                     => __( 'Catégories'),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => $this->option->getPluginPrefix().'tag' ),
        );
        register_taxonomy( $this->option->getPluginPrefix().'tag', $this->option->getPostType(), $args );
    }

	public function renderSingle($template)
	{
		$postName = $this->option->getPostType();
		global $post;
		if($post->post_type === $postName && $template !== locate_template(['single-'.$postName.'.php'])) {
			return $this->option->getPostTemplate();
		}
		return $template;
	}

	public function displayTags($args) {
		if($args === ''){
			$args = [];
		}
		$result = [];
		global $the_post;
		$tags = get_the_terms($the_post, $this->option->getPluginPrefix().'tag');
		foreach($tags as $tag) {
			$result[] = '<span class="rpb_tag">'.$tag->slug.'</span>';
		}
		return implode(($args['glue']?? ', '), $result);
	}

	public function addDefaultCSS()
	{
		$option = $this->option->getPluginOption('use_css');
		if($option ?? 'off' === 'on') {
			wp_register_style( 'rbp_default_css', $this->option->getRoot().'/assets/css/default.css' );
			wp_enqueue_style('rbp_default_css');
		}
	}
}