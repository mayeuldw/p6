<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 31/10/2018
 * Time: 14:55
 */

namespace dw_rpb_roleplayingbook;


class EntrySearch {

	private $option;
	private $metaQueryArgs = [];
	private $paged;
	private $param = [
	        'random' => false,
    ];

	public function __construct(Option $option)
	{
		$this->option = $option;
		add_filter( 'template_include', [$this, 'renderSearch']);

		add_shortcode('rpb_search_form', [$this, 'renderFormSearch']);
		add_shortcode('rpb_search_result', [$this, 'renderFormResult']);
		add_action( 'pre_get_posts', [$this, 'postMaxDisplay'] );

	}

	public function renderSearch($template) {
		global $wp_query;
		$post_type = get_query_var('post_type');
		if($wp_query->is_search && $post_type === $this->option->getPostType()) {
		    $template_file = locate_template('search-'.$this->option->getPostType().'.php');
		    if('' != $template_file) {
		        return $template_file;
            }
			return $this->option->getPostSearchTemplate();
		}
		return $template;
	}

	public function renderFormSearch($shortCodeArgs) {
	    if($shortCodeArgs !== ''){
	        foreach($shortCodeArgs as $s){
	            switch($s) {
                    case 'random':
                    case 'hide_links':
                    case 'hide_tags':
                    case 'hide_languages':
	                    $this->param[$s] = true;
	                    break;
                }
            }

        }
		$args = $this->getSearchArgs();

	    ob_start();
		echo do_shortcode('[chosen]');
		?>
		<form role="search" class="search-form rpb_form" method="<?= $args['method']?>" action="<?= site_url('/')?>" id="searchform">
			<input type="hidden" name="s" class="search-field" placeholder="Rechercher" value="<?= $args['s']?>">
            <input type="hidden" name="rbp_set" value="<?= base64_encode(json_encode($this->param)) ?>"?>
			<input type="hidden" class="search-field" name="post_type" value="<?= $args['post_type']?>">

            <select name="rbp_search[]" class="search-field rpb_search_field" multiple>
		        <?php if(!$this->param['hide_languages'] ?? false) { ?>
                <optgroup label="Langues">
		            <?php foreach($args['languages'] as $language) { ?>
                        <option value="<?= $language['id']?>" <?php if($language['selected']) { echo 'selected';}?>><?= $language['name']?></option>
		            <?php } ?>
                </optgroup>
        		<?php } ?>

                <?php if(!$this->param['hide_tags'] ?? false) { ?>
                <optgroup label="Tags">
		            <?php foreach($args['tags'] as $tag) { ?>
                        <option value="<?= $tag['id']?>" <?php if($tag['selected']) { echo 'selected';}?>><?= $tag['name']?></option>
		            <?php } ?>
                </optgroup>
    			<?php } ?>

		    	<?php if(!$this->param['hide_links'] ?? false) { ?>
        	    <optgroup label="Réseaux sociaux">
                <?php foreach($args['links'] as $link) { ?>
                    <option value="<?= $link['id']?>" <?php if($link['selected']) { echo 'selected';}?>><?= $link['name']?></option>
                <?php } ?>
                </optgroup>
    	    	<?php } ?>

            </select>

			<button type="submit" class="search-submit">
				Rechercher
			</button>
		</form>
		<?php
        return ob_get_clean();
	}

	public function renderFormResult(){
		global $wp_query;
		ob_start();

		$backUp_wp_query = $wp_query;
		$wp_query   = null;

		$wp_query = new \WP_Query($this->createMetaQueryArgs());
		if($this->param['random']) {
			$keepOnly = rand(1,$wp_query->found_posts);
		}

		$template_file = locate_template('search-'.$this->option->getPostType().'-result.php');
		if('' != $template_file) {
		    include $template_file;
		}
		else {
			include $this->option->getPostSearchResultTemplate();
        }

		$wp_query = null;
		$wp_query = $backUp_wp_query;
		return ob_get_clean();

	}

	public function postMaxDisplay(\WP_Query $query){
		$this->paged = $query->get('paged') ? $query->get('paged') : 1;
		if ( ! is_admin() && $query->get('post_type') === $this->option->getPostType()) {
		    $set = $_GET['rbp_set'] ?? $_POST['rbp_set'] ?? null;
		    if($set !== null) {
		        $this->param = json_decode(base64_decode($set), true);
		        if($this->param['random']) {
			        $query->set( 'posts_per_page', -1 );
			        return ;
                }
            }
            $query->set( 'posts_per_page', 5 );
		}
		return;
    }

    protected function getSearchArgs()
	{
		$usePost = ($this->param['post'] ?? false);
		$sp = $usePost ? $_POST:$_GET;


		global $s;
		$links = $this->option->getPluginOption('link_group');
		foreach($links as $k=>$link) {
			$links[$k]['key'] = $this->option->getPluginPrefix().'social_media_group_'.$link['id'];
			$links[$k]['selected'] = in_array($link['id'], $sp['rbp_search'] ?? []);
		}

		$languages = $this->option->getPluginOption('language_group');
		foreach($languages as $k=>$language) {
			$languages[$k]['key'] = $this->option->getPluginPrefix().'language_group_'.$language['id'];
			$languages[$k]['selected'] = in_array($language['id'], $sp['rbp_search'] ?? []);
		}

		$taxonomy = get_terms(['taxonomy' => 'rpb_tag', 'hide_empty' => true]);
		$t_array = [];
		foreach($taxonomy as $item) {
			$t_array[] = [
				'id'        => 'tax_'.$item->term_id,
				'key'       => $item->term_id,
				'name'      => $item->name,
				'selected'  => in_array('tax_'.$item->term_id, $sp['rbp_search'] ?? [])
			];
		}

		$args = [
			'method'    => $usePost ? 'post':'get',
			'post_type' => $this->option->getPostType(),
			's'         => $s,
			'links'     => $links,
			'languages' => $languages,
			'tags'      => $t_array
		];
		return $args;
	}


	/* Make WP QUERY*/
	protected function createMetaQueryArgs() {
	    $args = $this->getSearchArgs();
	    $this->metaQueryArgs = [];

	    // Add required Languages
	    $this->queryArgsLanguages($args);

	    // Add required Links
	    $this->queryArgsLinks($args);

        return $this->addTaxQuery($args, [
            'post_type'         => $args['post_type'],
            'post_per_page'     => 1,
            'paged'             => $this->paged,
            'orderby'           => 'DESC',
            'meta_query'        => $this->metaQueryArgs,
        ]);
    }

	protected function queryArgsLinks($args) {
		$check = [];
		foreach ($args['links'] as $link) {
			if($link['selected']) {
				$check[] = [
					'key' => $link['key'],
					'value' => '',
					'compare' => '!='
				];
			}
		}
		$this->listFilter('OR',$check);
	}

	protected function queryArgsLanguages($args) {
		$check = [];
		foreach ($args['languages'] as $language) {
			if($language['selected']) {
				$check[] = [
					'key' => $language['key'],
					'value' => '',
					'compare' => '!='
				];
			}
		}
		$this->listFilter('OR',$check);
	}

	protected function addTaxQuery($args, $query) {
	    $seletedTax = [];
        foreach($args['tags'] as $tag) {
            if($tag['selected']) {
	            $seletedTax[] = $tag['key'];
            }
        }

	    if(count($seletedTax) > 0) {
	        $query['tax_query'] = [[
		        'taxonomy'  => $this->option->getPluginPrefix().'tag',
		        'field'     => 'term_id',
		        'terms'     => $seletedTax,
		        'operator'  => 'IN'
            ]];
        }
	    return $query;
    }

    protected function listFilter($relation, $check) {
	    switch (count($check)) {
		    case 0:
		        break;
		    case 1:
			    $this->metaQueryArgs[] = $check[0];
			    break;
            default :
	            $check['relation'] = $relation;
	            $this->metaQueryArgs[] = $check;
	            break;
	    }
    }
}