<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 11/10/2018
 * Time: 12:28
 */

namespace dw_rpb_roleplayingbook;


class FontAwesome {

	private $option;

	public function __construct(Option $option)
	{
		$this->option = $option;
		add_action('admin_head', [$this, 'addToAdmin']);
		add_action('wp_head', [$this, 'addToPublicSite']);
	}

	public function addToAdmin()
	{
		$fa = $this->option->getPluginOption('fa-load');
		if(($fa[0]['fa-admin'] ?? 'off') === 'on') {
				$this->addFASources();
		}
	}

	public function addToPublicSite()
	{
		$fa = $this->option->getPluginOption('fa-load');
		if(($fa[0]['fa-public'] ?? 'off') === 'on') {
			$this->addFASources();
		}
	}

	public function addFASources()
	{
		wp_register_style( 'rpb_fontawesome_5_6', $this->option->getRoot().'/assets/fa-5-6-1/css/all.css');
		wp_enqueue_style('rpb_fontawesome_5_6');
	}
}