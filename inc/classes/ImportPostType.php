<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 07/11/2018
 * Time: 17:20
 */

namespace dw_rpb_roleplayingbook;


class ImportPostType {

	private $option;

	public function __construct(Option $option)
	{
		$this->option = $option;
		add_action('admin_head', [$this, 'addJSRessources']);
	}

	function addJSRessources()
	{
		wp_register_script( 'rpb_admin_script', $this->option->getRoot().'/assets/js/admin.js' );
		$dataSet = [
			'sel'   => 'b',
			'shift' => array_merge($this->getLanguageSettings(), $this->getLinksSettings()),
		];
		wp_localize_script( 'rpb_admin_script', 'dataset', $dataSet );
		wp_enqueue_script( 'rpb_admin_script' );
	}


	protected function getLanguageSettings()
	{
		$languages = $this->option->getPluginOption('language_group');
		$result = [];
		foreach ($languages as $language) {
			$result[$this->option->getPluginPrefix().'language_group_'.$language['id']] = $language['name'];
		}
		return $result;
	}

	protected function getLinksSettings()
	{
		$links = $this->option->getPluginOption('link_group');
		$result = [];
		foreach($links as $link) {
			$result[$this->option->getPluginPrefix().'social_media_group_'.$link['id']] = $link['name'];
		}
		return $result;
	}
}