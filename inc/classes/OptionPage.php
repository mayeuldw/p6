<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 11/10/2018
 * Time: 09:57
 */

namespace dw_rpb_roleplayingbook;

use CMB2;

class OptionPage {
	private $option;

	public function __construct(Option $option)
	{
		$this->option = $option;
		add_action('cmb2_admin_init', [ $this, 'registerThemeOptions' ] );
		add_action('pre_update_option_'.$this->optionName(), [ $this, 'checkBeforeChanges' ], 10, 2);
	}

	protected function optionName() {
		return $this->option->getPluginPrefix().'options';
	}

	function registerThemeOptions() {
        $option = $this->optionName();
		$cmb_options = new_cmb2_box( array(
			'id'           => $option.'_page',
			'menu_title'        => __($this->option->getPluginName()),
			'object_types' => ['options-page'],
			'option_key'   => $option,
            'tab_group'    => $option,
            'tab_title'    => __('Paramètres'),
			'capability'   => 'manage_options',
            'tab_title'    => __('Paramètres'),
		) );

		$this->addSlug($cmb_options);
		$this->addDefaultCSS($cmb_options);
		$this->addFontAwesome($cmb_options);
		$this->addLanguage($cmb_options);
		$this->addLink($cmb_options);

		new_cmb2_box([
			'id'           => $option.'_page2',
			'menu_title'   => __('Notice'),
			'object_types' => ['options-page'],
			'parent_slug'  => $option,
			'option_key'   => $option.'_help',
			'tab_group'    => $option,
			'tab_title'    => __('Notice'),
			'display_cb'   => [$this, 'displayHelp']
		]);
		new_cmb2_box([
			'id'           => $option.'_page3',
			'menu_title'   => __('Shortcodes'),
			'object_types' => ['options-page'],
			'parent_slug'  => $option,
			'option_key'   => $option.'_shortcode',
			'tab_group'    => $option,
			'tab_title'    => __('Shortcodes'),
			'display_cb'   => [$this, 'displayHelp']
		]);
		new_cmb2_box([
			'id'           => $option.'_page4',
			'menu_title'   => __('Templates'),
			'object_types' => ['options-page'],
			'parent_slug'  => $option,
			'option_key'   => $option.'_template',
			'tab_group'    => $option,
			'tab_title'    => __('Templates'),
			'display_cb'   => [$this, 'displayHelp']
		]);
		new_cmb2_box([
			'id'           => $option.'_page5',
			'menu_title'   => __('Importation CSV'),
			'object_types' => ['options-page'],
			'parent_slug'  => $option,
			'option_key'   => $option.'_import',
			'tab_group'    => $option,
			'tab_title'    => __('Importation CSV'),
			'display_cb'   => [$this, 'displayHelp']
		]);
	}

	function displayHelp($cmb_options ) {
		$tab_group = $cmb_options->cmb->prop( 'tab_group' );
		$tabs      = array();
		foreach ( \CMB2_Boxes::get_all() as $cmb_id => $cmb ) {
			if ( $tab_group === $cmb->prop( 'tab_group' ) ) {
				$tabs[ $cmb->options_page_keys()[0] ] = $cmb->prop( 'tab_title' )
					? $cmb->prop( 'tab_title' )
					: $cmb->prop( 'title' );
			}
		}
		?>
		<div class="wrap cmb2-options-page option-rpb_options">
			<?php if ( get_admin_page_title() ) : ?>
				<h2><?php echo wp_kses_post( get_admin_page_title() ); ?></h2>
			<?php endif; ?>
			<div class="wrap cmb2-options-page option-<?php echo $cmb_options->option_key; ?>">
				<h2 class="nav-tab-wrapper">
					<?php foreach ( $tabs as $option_key => $tab_title ) : ?>
						<a class="nav-tab<?php if ( isset( $_GET['page'] ) && $option_key === $_GET['page'] ) : ?> nav-tab-active<?php endif; ?>" href="<?php menu_page_url( $option_key ); ?>"><?php echo wp_kses_post( $tab_title ); ?></a>
					<?php endforeach; ?>
				</h2>
				<?php
					switch($_GET['page']) {
						case 'rpb_options_shortcode':
							include __DIR__ . '/../admin/notice/shortcode.php';
							break;
						case 'rpb_options_template':
							include __DIR__ . '/../admin/notice/template.php';
							break;
						case 'rpb_options_import':
							include __DIR__ . '/../admin/notice/import.php';
							break;

						case 'rpb_options_help':
						default:
						include __DIR__ . '/../admin/notice/read_me.php';
						break;

					}?>
			</div>
		</div>
		<?php
	}

	function addSlug(CMB2 $cmb) {
		$cmb->add_field([
			'name'    => __( 'Slug des entrées'),
			'desc'    => __( 'Permet de choisir l\'url de base des entrées de l\'annuaire.'),
			'id'      => 'slug',
			'type'    => 'text',
			'default' => 'rolling-book',
		]);
	}
	function addDefaultCSS(CMB2 $cmb) {
		$cmb->add_field([
			'name'    => __( 'Utilser la feuille CSS par défaut du plugin'),
			'desc'    => __( 'styles par défaut pour les shortcodes.'),
			'id'      => 'use_css',
			'type'    => 'checkbox',
			'default' => false,
		]);
	}

	function addFontAwesome(CMB2 $cmb) {
		$field_id = $cmb->add_field( array(
			'id'          => 'fa-load',
			'type'        => 'group',
			'repeatable'    => false,
			'options'     => array(
				'group_title'   => __( 'Font Awesome', 'cmb2' ),
				'closed'        => false,
			),
		) );

		$cmb->add_group_field($field_id, array(
			'name'    => __( 'Pages d\'administration'),
			'id'      => 'fa-admin',
			'desc'    => __( 'Active Font Awesome dans les pages d\'administration.<br>A activer seulement si les icones des réseaux sociaux ne s\'affichent pas dans l\'edition des entrées.'),
			'type'    => 'checkbox',
		) );

		$cmb->add_group_field($field_id, array(
			'name'    => __( 'Pages public'),
			'id'      => 'fa-public',
			'desc'    => __( 'Active Font Awesome sur le site public.<br>A activer seulement si les icones des réseaux sociaux ne s\'affichent pas sur le site publique.'),
			'type'    => 'checkbox',
		) );


	}

	function addLanguage(CMB2 $cmb) {
		$field_id = $cmb->add_field( array(
			'id'          => 'language_group',
			'type'        => 'group',
			'options'     => array(
				'group_title'   => __( 'Langue {#}', 'cmb2' ),
				'add_button'    => __( 'Add Another Entry', 'cmb2' ),
				'remove_button' => __( 'Remove Entry', 'cmb2' ),
				'sortable'      => true,
				'closed'        => false
			),
		) );

		$cmb->add_group_field($field_id, array(
			'id'   => 'id',
			'type' => 'hidden',
		) );

		$cmb->add_group_field($field_id, array(
			'name' => 'Nom',
			'id'   => 'name',
			'type' => 'text',
		) );

		$cmb->add_group_field($field_id, array(
			'name' => 'Icone',
			'id'   => 'icon',
			'type' => 'file',
		) );
	}

	function addLink(CMB2 $cmb) {
		$field_id = $cmb->add_field( array(
			'id'          => 'link_group',
			'type'        => 'group',
			'options'     => array(
				'group_title'   => __( 'Lien {#}', 'cmb2' ),
				'add_button'    => __( 'Add Another Entry', 'cmb2' ),
				'remove_button' => __( 'Remove Entry', 'cmb2' ),
				'sortable'      => true,
				'closed'        => false
			),
		) );

		$cmb->add_group_field($field_id, array(
			'id'   => 'id',
			'type' => 'hidden',
		) );

		$cmb->add_group_field($field_id, array(
			'name' => 'Nom',
			'id'   => 'name',
			'type' => 'text',
		) );
		$cmb->add_group_field($field_id, array(
			'name' => 'Url de base ',
			'id'   => 'baseUrl',
			'type' => 'text',
		) );
		$cmb->add_group_field($field_id, array(
			'name' => 'Prefix',
			'id'   => 'namePrefix',
			'type' => 'text',
		) );

		$cmb->add_group_field($field_id, array(
			'name' => 'Description',
			'id'   => 'desc',
			'type' => 'text',
		) );


		$cmb->add_group_field($field_id, array(
			'name' => __( 'Icone', 'cmb' ),
			'id'   => 'icon',
			'desc' => 'Font Awesome 5.6. <a target="_blank" href="https://fontawesome.com/v5.6.1/icons/">Liste des icones disponibles</a>',
			'type' => 'text',
		) );
	}

	function checkBeforeChanges($new, $old) {
		foreach($new['language_group'] as $k=>$language) {
			if($language['name'] === '') {
				unset($new['language_group'][$k]);
				continue;
			}
			if(!isset($language['id']) || $language['id'] === '') {
				$new['language_group'][$k]['id'] = $this->generateId();
			}
		}
		foreach($new['link_group'] as $k=>$link) {
			if($link['name'] === '') {
				unset($new['link_group'][$k]);
				continue;
			}
			if(!isset($link['id']) || $link['id'] === '') {
				$new['link_group'][$k]['id'] = $this->generateId();
			}
		}
		return $new;
	}

	private function generateId()
	{
		return str_replace('.', '', uniqid('', true));
	}
}