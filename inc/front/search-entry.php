<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

    get_header();
?>
	<div class="wrap">
	<div id="primary" class="content-area">
		<?php
            echo do_shortcode('[chosen]');
			echo do_shortcode('[rpb_search_form]');
        ?>
		<br>
		<br>
			<header class="page-header">
				<?php if ( have_posts() ) : ?>
					<h1 class="page-title"><?php  _e( 'Search Results'); ?></h1>
				<?php else : ?>
					<h1 class="page-title"><?php _e( 'Nothing Found'); ?></h1>
				<?php endif; ?>
			</header><!-- .page-header -->
			<main id="main" class="site-main" role="main">
                <?php
                    echo do_shortcode('[rpb_search_result]');
                ?>
			</main><!-- #main -->
		</div><!-- #primary -->
		<?php  //get_sidebar(); ?>
	</div><!-- .wrap -->
<?php
    get_footer();
