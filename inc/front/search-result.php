<?php
    /** @var int $keepOnly */
    $i = 1;
if ( have_posts() ) :
	/* Start the Loop */
	while ( have_posts() ) : the_post();

	if($keepOnly === null || $keepOnly === $i){
		/**
		 * Run the loop for the search to output the results.
		 * If you want to overload this in a child theme then include a file
		 * called content-search.php and that will be used instead.
		 */

		?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <header class="entry-header">
				<?php if ( is_front_page() && ! is_home() ) {

					// The excerpt is being displayed within a front page section, so it's a lower hierarchy than h2.
					the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );
				} else {
					the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
				} ?>
            </header><!-- .entry-header -->

            <div class="entry-summary">
				<?php the_excerpt(); ?>
            </div><!-- .entry-summary -->

        </article><!-- #post-## -->
        <?php
    }
    $i++;

	endwhile; // End of the loop.

	the_posts_pagination( array(
		'prev_text' => '<span class="screen-reader-text">' . __( 'Previous page') . '</span>',
		'next_text' => '<span class="screen-reader-text">' . __( 'Next page') . '</span>',
		'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page') . ' </span>',
	) );

	if($keepOnly !== null) :
        echo '<button class="search-submit" onclick="window.location.reload()">'.__('Encore!').'</button>';
	endif;

else :
	?>

	<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.'); ?></p>
	<?php

endif;
